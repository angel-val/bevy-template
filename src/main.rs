use bevy::{
    prelude::*,
    window::{PresentMode, WindowMode, WindowTheme},
};
use eyre::Result;

const WINDOW_TITLE: &str = "Game";
const WINDOW_RES: (f32, f32) = (640., 480.);

#[warn(clippy::pedantic, clippy::unwrap_used)]
#[allow(
    clippy::wildcard_imports,
    clippy::must_use_candidate,
    clippy::needless_pass_by_value,
    clippy::type_complexity
)]
fn main() -> Result<()> {
    color_eyre::install()?;
    let mut app = App::default();
    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
            title: WINDOW_TITLE.to_string(),
            resolution: WINDOW_RES.into(),
            present_mode: PresentMode::Immediate,
            window_theme: Some(WindowTheme::Dark),
            mode: WindowMode::Windowed,
            ..default()
        }),
        ..default()
    }));
    #[cfg(debug_assertions)]
    app.add_systems(Update, bevy::window::close_on_esc);
    app.add_plugins(b13::game::Game);
    app.run();
    Ok(())
}
