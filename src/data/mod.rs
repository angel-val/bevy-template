use crate::*;

// Generic trait for reading a serializable struct from a file
pub trait ImportFromFile {
    fn from_file(path: &str) -> Result<Self>
    where
        Self: Sized;
}
impl<T> ImportFromFile for T
where
    T: Sized + Serialize + for<'a> Deserialize<'a>,
{
    fn from_file(path: &str) -> Result<Self> {
        let yaml = std::fs::read_to_string(path).wrap_err("Invalid file location")?;
        serde_yaml::from_str::<T>(&yaml).wrap_err("Invalid data")
    }
}
